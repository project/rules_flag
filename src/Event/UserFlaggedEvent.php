<?php

namespace Drupal\rules_flag\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\user\UserInterface;
use Drupal\flag\FlaggingInterface;

/**
 * Event that is fired when a user is flagged.
 *
 * @see rules_flag_flagging_insert()
 */
class UserFlaggedEvent extends Event {

  const EVENT_NAME = 'rules_flag_user_flagged';

  /**
   * The Flagging entity.
   *
   * @var \Drupal\flag\FlaggingInterface
   */
  public $flagging;

  /**
   * The User is flagged.
   *
   * @var \Drupal\user\UserInterface
   */
  public $user;

  /**
   * Constructs the object.
   *
   * @param \Drupal\flag\FlaggingInterface $flagging
   *   The flagging entity.
   * @param \Drupal\user\UserInterface $user
   *   The flagging user.
   */
  public function __construct(FlaggingInterface $flagging, UserInterface $user) {
    $this->flagging = $flagging;
    $this->user = $user;
  }

}
