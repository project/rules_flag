<?php

namespace Drupal\rules_flag\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a generic 'Delete scheduled entity' action.
 *
 * @RulesAction(
 *   id = "rules_flag_delete_scheduled_entity",
 *   label = @Translation("Delete scheduled entity"),
 *   category = @Translation("Entity"),
 *   context_definitions = {
 *    "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the entity, which should be deleted permanently."),
 *       assignment_restriction = "selector",
 *     ),
 *     "period" = @ContextDefinition("string",
 *       label = @Translation("Period"),
 *       description = @Translation("The period measured in the number of seconds."),
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 */
class EntityScheduleDelete extends RulesActionBase {

  /**
   * Deletes the Entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be deleted.
   * @param int $period
   *   The period to execute the job.
   */
  protected function doExecute(EntityInterface $entity, $period) {
    $type = $entity->getEntityTypeId();
    $id = $entity->id();
    $job = [
      'name' => 'rules_flag_entity_delete_scheduler',
      'type' => $type,
      'id' => $id,
      'period' => $period,
      'periodic' => FALSE,
    ];
    $service = \Drupal::service('job_scheduler.manager');
    $service->set($job);
  }

}
