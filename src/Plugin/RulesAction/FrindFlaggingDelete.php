<?php

namespace Drupal\rules_flag\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\flag\Entity\Flag;

/**
 * Provides a generic 'Delete friend flagging' action.
 *
 * @RulesAction(
 *   id = "rules_flag_delete_friend_flagging",
 *   label = @Translation("Delete friend flagging"),
 *   category = @Translation("Flag"),
 *   context_definitions = {
 *    "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the flagging used to find the friend."),
 *       assignment_restriction = "selector",
 *     ),
 *   }
 * )
 */
class FrindFlaggingDelete extends RulesActionBase {

  /**
   * Deletes the friend flagging.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity used to find the friend.
   */
  protected function doExecute(EntityInterface $entity) {
    $flag_id =$entity->getFlagId();
    $flag = Flag::load($flag_id);
    $owner = $entity->getOwner();
    $flagged_entity = $entity->get('flagged_entity')->referencedEntities();
    if (!empty($flagged_entity)) {
      $friend = reset($flagged_entity);
      $friend_id = $friend->id();
    }
    $flag_service = \Drupal::service('flag');
    $friend_flaggings = $flag_service->getEntityFlaggings($flag, $owner, $friend);
    foreach ($friend_flaggings as $friend_flagging) {
      $friend_owner_id = $friend_flagging->getOwnerId();
      if ($friend_owner_id == $friend_id) {
        $friend_flagging->delete();
        return;
      }
    }
  }

}
