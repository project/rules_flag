<?php

namespace Drupal\rules_flag\Plugin\Condition;

use Drupal\flag\FlaggingInterface;
use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides an 'Flag has ID' condition.
 *
 * @Condition(
 *   id = "rules_flag_has_id",
 *   label = @Translation("Flag has ID"),
 *   category = @Translation("Flag"),
 *   context_definitions = {
 *     "flagging" = @ContextDefinition("entity",
 *       label = @Translation("Flagging"),
 *       description = @Translation("Specifies the flagging for which to evaluate the condition."),
 *       assignment_restriction = "selector"
 *     ),
 *     "flag_id" = @ContextDefinition("string",
 *       label = @Translation("Flag ID"),
 *       description = @Translation("The Flag ID to check for."),
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 *
 */
class FlagHasId extends RulesConditionBase {

  /**
   * Check if Flag has a particular ID.
   *
   * @param \Drupal\flag\FlaggingInterface $flagging
   *   The Flagging entity.
   * @param string $flag_id
   *   The flag ID to check for.
   *
   * @return bool
   *   TRUE if the flag ID matches.
   */
  protected function doEvaluate(FlaggingInterface $flagging, string $flag_id) {
    $flagging_flag_id = $flagging->getFlagId();
    return $flagging_flag_id == $flag_id;
  }

}
