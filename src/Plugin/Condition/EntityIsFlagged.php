<?php

namespace Drupal\rules_flag\Plugin\Condition;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rules\Core\RulesConditionBase;
use Drupal\user\Entity\User;

/**
 * Provides an 'Entity is flagged' condition.
 *
 * @Condition(
 *   id = "rules_entity_is_flagged",
 *   label = @Translation("Entity is flagged"),
 *   category = @Translation("Entity"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("Specifies the entity for which to evaluate the condition."),
 *       assignment_restriction = "selector"
 *     ),
 *     "flag" = @ContextDefinition("string",
 *       label = @Translation("flag"),
 *       description = @Translation("The Flag ID."),
 *       assignment_restriction = "input"
 *     ),
 *     "uid" = @ContextDefinition("integer",
 *       label = @Translation("uid"),
 *       description = @Translation("The user ID who has flagged it."),
 *       assignment_restriction = "input"
 *     ),
 *   }
 * )
 *
 */
class EntityIsFlagged extends RulesConditionBase {

  /**
   * Check if the provided entity is flagged.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check if it is flagged.
   * @param string $flag
   *   The flag to check for.
   * @param int $uid
   *   The uid of the user who has flagged it.
   *
   * @return bool
   *   TRUE if the entity is flagged.
   */
  protected function doEvaluate(EntityInterface $entity, string $flag, int $uid) {
    $flag_id = $flag;
    /** @var \Drupal\flag\FlagService $flag_service */
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById($flag_id);
    if (empty($flag)) {
      return FALSE;
    }

    $account = User::load($uid);
    if (is_null($account)) {
      return FALSE;
    }

    $flagging = $flag_service->getFlagging($flag, $entity, $account);
    if (is_null($flagging)) {
      return FALSE;
    }

    return TRUE;
  }

}
