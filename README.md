# Rules Flag

This module provides integration of 'Rules' and 'Flag' modules. It adds rules 
events after flagging or unflagging an entity, and the event action to create
a new flagging entity. It adds rules examples with the events.

## rules events

It provides rules events after flagging an entity with two context variables, 
one is flagging entity, the other one is the entity flagged.


 - After flagging a content item
 - After flagging a user
 - ...
 - After unflagging a user
 - ...

## rules actions

 - Create a new flagging entity
 - Delete scheduled entity

## rules examples

It provides rules examples with the events, when install the module, these 
example will be imported with disabled status.

 - Show a message when the article is flagged
 - Show a message when the user is flagged
 - Show a message when the user is flagged
 - Show a message when the user is unflagged
 - Flag the article when it is created
 - Flag the article when it is created and unflag it in schedule
